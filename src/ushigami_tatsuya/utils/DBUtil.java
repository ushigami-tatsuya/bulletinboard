package ushigami_tatsuya.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import ushigami_tatsuya.exception.SQLRuntimeException;



public class DBUtil {
	
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost/ushigami_tatsuya";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    static {

        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    //コネクション取得
    public static Connection getConnection() {

        try {
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);

            connection.setAutoCommit(false);

            return connection;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    //コミット
    public static void commit(Connection connection) {

        try {
            connection.commit();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }
    
    //ロールバック
    public static void rollback(Connection connection) {

        if (connection == null) {
            return;
        }

        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }
}
