package ushigami_tatsuya.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ushigami_tatsuya.service.MessageService;



@WebServlet(urlPatterns = { "/deletemessage" })
public class DeleteMessageServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		String messageID = request.getParameter("message_id");
		int messageId = Integer.parseInt(messageID);
		
		new MessageService().messageId(messageId);

		response.sendRedirect("./");
	}
}
