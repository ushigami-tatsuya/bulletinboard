package ushigami_tatsuya.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ushigami_tatsuya.beans.Comment;
import ushigami_tatsuya.beans.User;
import ushigami_tatsuya.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	Comment comment  = new Comment();
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();


		if (isValid(request, messages)) {

			User user = (User) session.getAttribute("loginUser");


			comment.setText(request.getParameter("text"));
			comment.setUserId(user.getId());
			String messageID = request.getParameter("messageid");
			comment.setMessageId(Integer.parseInt(messageID));

			new CommentService().register(comment);

			response.sendRedirect("./");

		} else {
			comment.setText(request.getParameter("text"));
			session.setAttribute("errorMessages", messages);
			session.setAttribute("inittext", comment.getText());
			response.sendRedirect("./");
		}
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String text = request.getParameter("text");

		if (StringUtils.isBlank(text)){
			messages.add("投稿内容を入力してください");
		}else {
			if (501 < text.length()) {
				messages.add("本文は500文字以下で入力してください");
			}
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
