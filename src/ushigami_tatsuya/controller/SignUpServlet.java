package ushigami_tatsuya.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import ushigami_tatsuya.beans.Branch;
import ushigami_tatsuya.beans.Department;
import ushigami_tatsuya.beans.User;
import ushigami_tatsuya.service.BranchService;
import ushigami_tatsuya.service.DepartmentService;
import ushigami_tatsuya.service.UserService;


@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

	List<Branch> branches = new BranchService().getBranch();
	List<Department> departments = new DepartmentService().getDepartment();

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {



		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages)) {
			User user = new User();
			user.setAccount(request.getParameter("account"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			String branchID = request.getParameter("branch_id");
			user.setBranchId(Integer.parseInt(branchID));
			String departmentID = request.getParameter("department_id");
			user.setDepartmentId(Integer.parseInt(departmentID));
			new UserService().register(user);

			response.sendRedirect("manegement");

		} else {
			request.setAttribute("errorMessages", messages);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordcheck");
		String name = request.getParameter("name");
		String branchId = request.getParameter("branch_id");
		String departmentId = request.getParameter("department_id");

		boolean  isDuplication  =  new UserService().getUserSignup(account);

		if (StringUtils.isBlank(account)) {
			messages.add("アカウント名が入力されていません");
		}else {
			if(account.length() < 6) {
				messages.add("アカウントは6文字以上入力してください");
			}else if(account.length() > 20) {
				messages.add("アカウントは20文字以下で入力してください");
			}
		}
		if (isDuplication == false) {
			messages.add("アカウントが重複しています");
		}
		if (StringUtils.isBlank(password)) {
			messages.add("パスワードが入力されていません");
		}else {
			if(password.length() < 6) {
				messages.add("パスワードは6文字以上で入力してください");
			}
		}
		if(StringUtils.isBlank(passwordCheck)) {
			messages.add("確認用パスワードが入力されていません");
		}
		if(!StringUtils.isBlank(password) && !StringUtils.isBlank(passwordCheck)) {
			if(!password.equals(passwordCheck)){
				messages.add("パスワードが一致しません");
			}
		}
		if (StringUtils.isBlank(name)) {
			messages.add("名前が入力されていません");
		}else {
			if(name.length() > 10) {
				messages.add("ユーザー名は10文字以下で入力してください");
			}
		}
		if(branchId.equals("1") && (departmentId.equals("3") || departmentId.equals("4"))) {
			messages.add("支社と部署の組み合わせが不正です");
		}
		if(branchId.equals("2") && (departmentId.equals("1") || departmentId.equals("2"))) {
			messages.add("支社と部署の組み合わせが不正です");
		}

		if (messages.size() == 0) {
			return true;
		}
		return false;
	}
}
