package ushigami_tatsuya.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ushigami_tatsuya.beans.Branch;
import ushigami_tatsuya.beans.Department;
import ushigami_tatsuya.beans.User;
import ushigami_tatsuya.service.BranchService;
import ushigami_tatsuya.service.DepartmentService;
import ushigami_tatsuya.service.UserService;


	@WebServlet(urlPatterns = { "/setting" })
	public class SettingServlet extends HttpServlet {

	    List<Branch> branches = new BranchService().getBranch();
	    List<Department> departments = new DepartmentService().getDepartment();
	    User user2 = new User();

		    @Override
		    protected void doGet(HttpServletRequest request,
		            HttpServletResponse response) throws IOException, ServletException {

		    	String ID =request.getParameter("userId");
		    	HttpSession session = request.getSession();
		    	int id = 0;
		        try {
		        	User user = (User) session.getAttribute("loginUser");
		            id = Integer.parseInt(ID);
			    	UserService userService = new UserService();
			    	user2 = userService.getUser2(id);
			    	user.getId();
			    	boolean presence = new UserService().getPresence(ID);

			    	if(presence) {
			    	request.setAttribute("user", user);
			    	request.setAttribute("init", user2);
		    	    request.setAttribute("branches", branches);
		    	    request.setAttribute("departments", departments);
		    	    request.getRequestDispatcher("setting.jsp").forward(request, response);
			    	}else {
		            	List<String> messages = new ArrayList<String>();
			            messages.add("不正なパラメータが入力されました");
			            session.setAttribute("messages", messages);

						response.sendRedirect("manegement");
			    	}

		            } catch (NumberFormatException e) {
		            	List<String> messages = new ArrayList<String>();
			            messages.add("不正なパラメータが入力されました");
			            session.setAttribute("messages", messages);

						response.sendRedirect("manegement");
		        }


		    }


			@Override
			protected void doPost(HttpServletRequest request,
					HttpServletResponse response) throws IOException, ServletException {

				List<String> messages = new ArrayList<String>();
				User user = new User();

				String ID = request.getParameter("id");
				user.setId(Integer.parseInt(ID));
				user.setAccount(request.getParameter("account"));
				user.setPassword(request.getParameter("password"));
				user.setName(request.getParameter("name"));
				String branchID = request.getParameter("branch_id");
				user.setBranchId(Integer.parseInt(branchID));
				String departmentID = request.getParameter("department_id");
				user.setDepartmentId(Integer.parseInt(departmentID));

				if (isValid(request, messages)) {


					new UserService().userEdit(user);

					response.sendRedirect("manegement");

				} else {
					request.setAttribute("errorMessages", messages);
		    	    request.setAttribute("branches", branches);
		    	    request.setAttribute("departments", departments);
		    	    request.setAttribute("init", user);
					request.getRequestDispatcher("setting.jsp").forward(request, response);
				}
			}

		private boolean isValid(HttpServletRequest request, List<String> messages) {
			String account = request.getParameter("account");
			String password = request.getParameter("password");
			String passwordCheck = request.getParameter("passwordcheck");
			String name = request.getParameter("name");
			String branchId = request.getParameter("branch_id");
			String departmentId = request.getParameter("department_id");

			int id = init.getId();
			boolean  isDuplication  =  new UserService().getUserUpdate(account, id);

			if (StringUtils.isBlank(account)) {
				messages.add("アカウント名が入力されていません");
			}else {
				if(account.length() < 6) {
					messages.add("アカウントは6文字以上入力してください");
				}else if(account.length() > 20) {
					messages.add("アカウントは20文字以下で入力してください");
				}
			}
			if (isDuplication == false) {
				messages.add("アカウントが重複しています");
			}
			if(!StringUtils.isBlank(password) && password.length() < 6) {
					messages.add("パスワードは6文字以上で入力してください");
				}
			if(!StringUtils.isBlank(password) && !StringUtils.isBlank(passwordCheck)) {
				if(!password.equals(passwordCheck)){
					messages.add("パスワードが一致しません");
				}
			}
			if (StringUtils.isBlank(name)) {
				messages.add("名前が入力されていません");
			}else {
				if(name.length() > 10) {
					messages.add("ユーザー名は10文字以下で入力してください");
				}
			}
			if(branchId.equals("1") && (departmentId.equals("3") || departmentId.equals("4"))) {
				messages.add("支社と部署の組み合わせが不正です");
			}
			if(branchId.equals("2") && (departmentId.equals("1") || departmentId.equals("2"))) {
				messages.add("支社と部署の組み合わせが不正です");
			}

			if (messages.size() == 0) {
				return true;
			}
			return false;
		}
	}
