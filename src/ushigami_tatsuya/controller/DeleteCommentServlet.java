package ushigami_tatsuya.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ushigami_tatsuya.service.CommentService;


@WebServlet(urlPatterns = { "/deletecomment" })
public class DeleteCommentServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		
		String commentsID = request.getParameter("comments_id");
		int commentsId = Integer.parseInt(commentsID);
		
		new CommentService().commentsId(commentsId);

		response.sendRedirect("./");
	}
}
