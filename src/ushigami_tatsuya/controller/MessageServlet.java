package ushigami_tatsuya.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ushigami_tatsuya.beans.Message;
import ushigami_tatsuya.beans.User;
import ushigami_tatsuya.service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("message.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

		if (isValid(request, messages)) {

			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setTitle(request.getParameter("title"));
			message.setText(request.getParameter("text"));
			message.setCategory(request.getParameter("category"));
			message.setUserId(user.getId());

			new MessageService().register(message);

			response.sendRedirect("./");
		} else {
			Message initmessage = new Message();
			initmessage.setTitle(request.getParameter("title"));
			initmessage.setText(request.getParameter("text"));
			initmessage.setCategory(request.getParameter("category"));

			session.setAttribute("errorMessages", messages);
			session.setAttribute("init", initmessage);
			response.sendRedirect("message");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if (StringUtils.isBlank(title)) {
			messages.add("件名を入力してください");
		}else {
			if (30 < title.length()) {
				messages.add("件名は30文字以下で入力してください");
			}
		}
		if (StringUtils.isBlank(text)) {
			messages.add("投稿内容を入力してください");
		}else {
			if (1000 < text.length()) {
				messages.add("本文は1000文字以下で入力してください");
			}
		}
		if (StringUtils.isBlank(category)) {
			messages.add("カテゴリーを入力してください");
		}else {
			if (10 < category.length()) {
				messages.add("カテゴリーは10文字以下で入力してください");
			}
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}