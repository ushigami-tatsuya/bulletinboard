package ushigami_tatsuya.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ushigami_tatsuya.beans.User;
import ushigami_tatsuya.beans.UserBranchDepartment;
import ushigami_tatsuya.service.UserService;

@WebServlet(urlPatterns = { "/manegement" })
public class ManegementServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
	HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();
    	User user = (User) session.getAttribute("loginUser");
		List<UserBranchDepartment> manegements = new UserService().getManegement();

		Object messages = session.getAttribute("messages");
		request.setAttribute("errorMessages", messages);

		request.setAttribute("user", user);
		request.setAttribute("manegements", manegements);

		request.getRequestDispatcher("manegement.jsp").forward(request, response);
	}
}

