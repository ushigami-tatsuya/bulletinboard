package ushigami_tatsuya.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ushigami_tatsuya.beans.User;
import ushigami_tatsuya.service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		User user = new User();
		
		String ID = request.getParameter("userid");
			int id = Integer.parseInt(ID);
		user.setId(id);
		
		String stopID = request.getParameter("userisstopped");
		user.setIsStopped(Integer.parseInt(stopID));
		new UserService().userIdStop(user);

		response.sendRedirect("manegement");
	}
}
