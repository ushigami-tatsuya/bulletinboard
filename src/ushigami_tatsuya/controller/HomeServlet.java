package ushigami_tatsuya.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ushigami_tatsuya.beans.User;
import ushigami_tatsuya.beans.UserComment;
import ushigami_tatsuya.beans.UserMessage;
import ushigami_tatsuya.service.CommentService;
import ushigami_tatsuya.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
    HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();
    	User user = (User) session.getAttribute("loginUser");

		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String category = request.getParameter("category");

		List<UserMessage> messages = new MessageService().getMessage(start, end, category);
        List<UserComment> comments = new CommentService().getComment();


        request.setAttribute("user", user);
        request.setAttribute("messages", messages);
        request.setAttribute("comments", comments);
        request.setAttribute("start",start );
        request.setAttribute("end", end);
        request.setAttribute("category", category);

        request.getRequestDispatcher("home.jsp").forward(request, response);
    }
}