package ushigami_tatsuya.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ushigami_tatsuya.beans.User;
import ushigami_tatsuya.service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {
    		HttpSession session = request.getSession();
    		Object messages = session.getAttribute("messages");
    		request.setAttribute("errorMessages", messages);
    		request.getRequestDispatcher("login.jsp").forward(request, response);
    	}

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String account = request.getParameter("account");
        String password = request.getParameter("password");

        UserService userService = new UserService();
        User user = userService.login(account, password);

        HttpSession session = request.getSession();
        if (user != null) {
        	if (user.getIsStopped() == 1 ) {
                List<String> messages = new ArrayList<String>();
                messages.add("アカウントまたはパスワードが誤っています");
                request.setAttribute("errorMessages", messages);
                session.invalidate();
                request.getRequestDispatcher("login.jsp").forward(request, response);
        	}else {
            session.setAttribute("loginUser", user);
            response.sendRedirect("./");
        	}
        } else {
        	List<String> messages = new ArrayList<String>();
        	if(StringUtils.isEmpty(account)) {
                messages.add("アカウントが入力されていません");
        	}
        	if(StringUtils.isEmpty(password)){
                messages.add("パスワードが入力されていません");
        	}
        	if(!StringUtils.isEmpty(account) && (!StringUtils.isEmpty(password))){
        			messages.add("アカウントまたはパスワードが誤っています");
        	}
        		request.setAttribute("errorMessages", messages);
        		request.setAttribute("init", account);
        		request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
}