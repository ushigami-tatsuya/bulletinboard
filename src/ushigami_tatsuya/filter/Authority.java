package ushigami_tatsuya.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ushigami_tatsuya.beans.User;

@WebFilter(filterName = "authority" , urlPatterns={"/manegement/*","/setting/*","/signup/*"})
public class Authority implements Filter {


	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain)throws IOException, ServletException{

		HttpSession session = ((HttpServletRequest)request).getSession();
		User user = (User) session.getAttribute("loginUser");

		if(user != null && user.getBranchId() == 1  && user.getDepartmentId() == 1) {
			chain.doFilter(request, response);
		}else {
			((HttpServletResponse)response).sendRedirect("./");
		}

	}

	@Override
	public void init(FilterConfig config) throws ServletException{}

	@Override
	public void destroy() {}
}

