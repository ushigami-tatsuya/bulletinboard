package ushigami_tatsuya.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ushigami_tatsuya.beans.User;

@WebFilter(filterName = "loginFilter" , urlPatterns="/*")
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain)throws ServletException, IOException{

		String requestUrl = ((HttpServletRequest) request).getServletPath().toString();
		String loginUrl = "/login";
		String cssUrl = "css";



		if(requestUrl.equals(loginUrl) || requestUrl.matches(cssUrl)){
			chain.doFilter(request, response);
		}else {
			HttpSession session = ((HttpServletRequest)request).getSession();
			User user = new User();
			user = null;
			if(session != null) {
				user = (User) session.getAttribute("loginUser");
			}

			if(user != null) {
				chain.doFilter(request, response);
			}else {
	            List<String> messages = new ArrayList<String>();
	            messages.add("ログインしてください");
	            HttpSession session2 = ((HttpServletRequest)request).getSession();
	            session2.setAttribute("messages", messages);

				((HttpServletResponse)response).sendRedirect("login");
			}
		}
	}



	@Override
	public void init(FilterConfig config) throws ServletException{}

	@Override
	public void destroy() {}
}