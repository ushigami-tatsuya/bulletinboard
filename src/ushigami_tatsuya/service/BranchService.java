package ushigami_tatsuya.service;

import static ushigami_tatsuya.utils.CloseableUtil.*;
import static ushigami_tatsuya.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ushigami_tatsuya.beans.Branch;
import ushigami_tatsuya.dao.BranchDao;

public class BranchService {

	public List<Branch> getBranch() {

		Connection connection = null;
		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();
			List<Branch> ret =  branchDao.getBranch(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
