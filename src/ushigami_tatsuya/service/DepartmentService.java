package ushigami_tatsuya.service;

import static ushigami_tatsuya.utils.CloseableUtil.*;
import static ushigami_tatsuya.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ushigami_tatsuya.beans.Department;
import ushigami_tatsuya.dao.DepartmentDao;


public class DepartmentService {

	public List<Department> getDepartment() {

		Connection connection = null;
		try {
			connection = getConnection();

			DepartmentDao departmentDao = new DepartmentDao();
			List<Department> ret =  departmentDao.getDepartment(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
