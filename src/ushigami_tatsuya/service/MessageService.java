package ushigami_tatsuya.service;

import static ushigami_tatsuya.utils.CloseableUtil.*;
import static ushigami_tatsuya.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import ushigami_tatsuya.beans.Message;
import ushigami_tatsuya.beans.UserMessage;
import ushigami_tatsuya.dao.MessageDao;
import ushigami_tatsuya.dao.UserMessageDao;

public class MessageService {

    public void register(Message message) {

    	Connection connection = null;
    	try {
    		 connection = getConnection();
    		 MessageDao messageDao = new MessageDao();
    		 messageDao.insert(connection, message);

    		 commit(connection);
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

	private static final int LIMIT_NUM = 1000;

	public List<UserMessage> getMessage(String start, String end, String category) {

		Connection connection = null;
		try {
			connection = getConnection();
			String startDate = null;
			String endDate = null;
			String categoryData = null;


			if(!StringUtils.isEmpty(start)) {
				startDate = (start) + " 00:00:00";
			}else {
				startDate = "2019-01-01 00:00:00";
			}

			if(!StringUtils.isEmpty(end)) {
				endDate = (end) + " 23:59:59";
			}else {
		        Calendar cal = Calendar.getInstance();
		        SimpleDateFormat END = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		        endDate = String.valueOf(END.format(cal.getTime()));
			}

			if(!StringUtils.isEmpty(category)) {
				categoryData = "%" + category + "%";
			}

			UserMessageDao messageDao = new UserMessageDao();
			List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM, startDate, endDate, categoryData);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
        	rollback(connection);
        	throw e;
		} finally {
			close(connection);
		}
	}

    public void messageId(int messageId) {

    	Connection connection = null;
    	try {
    		 connection = getConnection();
    		 MessageDao messageDao = new MessageDao();
    		 messageDao.delete(connection, messageId);

    		 commit(connection);
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
}