package ushigami_tatsuya.service;

import static ushigami_tatsuya.utils.CloseableUtil.*;
import static ushigami_tatsuya.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import ushigami_tatsuya.beans.User;
import ushigami_tatsuya.beans.UserBranchDepartment;
import ushigami_tatsuya.dao.UserBranchDepartmentDao;
import ushigami_tatsuya.dao.UserDao;
import ushigami_tatsuya.utils.CipherUtil;

public class UserService {

	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public boolean getUserSignup(String account) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			boolean isDuplication = userDao.isDuplicationSignup(connection, account);

			commit(connection);

			return isDuplication;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public boolean getUserUpdate(String account, int id) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			boolean isDuplication = userDao.isDuplicationUpdate(connection, account, id);

			commit(connection);

			return isDuplication;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User login(String account, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, account, encPassword);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<UserBranchDepartment> getManegement() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserBranchDepartmentDao manegementDao = new UserBranchDepartmentDao();
			List<UserBranchDepartment> ret = manegementDao.getUserBranchDepartment(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
        	rollback(connection);
        	throw e;
		} finally {
			close(connection);
		}
	}

	public void userEdit(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			if(!StringUtils.isEmpty(user.getPassword())) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void userIdStop(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			userDao.stopped(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User getUser2(int id) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.getUser2(connection, id);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public boolean getPresence(String ID) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			boolean presence = userDao.getPresence(connection, ID);

			commit(connection);

			return presence;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
