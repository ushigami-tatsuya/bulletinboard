package ushigami_tatsuya.service;

import static ushigami_tatsuya.utils.CloseableUtil.*;
import static ushigami_tatsuya.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ushigami_tatsuya.beans.Comment;
import ushigami_tatsuya.beans.UserComment;
import ushigami_tatsuya.dao.CommentDao;
import ushigami_tatsuya.dao.UserCommentDao;


public class CommentService {

	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();
			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	private static final int LIMIT_NUM = 1000;

	public List<UserComment> getComment() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserCommentDao commentDao = new UserCommentDao();
			List<UserComment> ret = commentDao.getUserComment(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	
    public void commentsId(int commentsId) {

    	Connection connection = null;
    	try {
    		 connection = getConnection();
    		 CommentDao commentDao = new CommentDao();
    		 commentDao.delete(connection, commentsId);

    		 commit(connection);
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
}
