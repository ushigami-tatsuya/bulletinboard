package ushigami_tatsuya.dao;

import static ushigami_tatsuya.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ushigami_tatsuya.beans.UserComment;
import ushigami_tatsuya.exception.SQLRuntimeException;

public class UserCommentDao {

	 public List<UserComment> getUserComment(Connection connection, int num) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("comments.id as id, ");
	            sql.append("users.name as name, ");
	            sql.append("comments.text as text, ");
	            sql.append("comments.message_id as message_id, ");
	            sql.append("comments.user_id as user_id, ");
	            sql.append("comments.created_date as created_date, ");
	            sql.append("comments.updated_date as updated_date ");
	            sql.append("FROM comments ");
	            sql.append("INNER JOIN users ");
	            sql.append("ON users.id = comments.user_id ");
	            sql.append("ORDER BY created_date ASC limit " + num);

	            ps = connection.prepareStatement(sql.toString());
	            ResultSet rs = ps.executeQuery();

	            List<UserComment> ret = toUserCommentList(rs);

	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	    private List<UserComment> toUserCommentList(ResultSet rs)
	            throws SQLException {

	        List<UserComment> ret = new ArrayList<UserComment>();
	        try {
	            while (rs.next()) {
	            	int id = rs.getInt("id");
	                String name = rs.getString("name");
	                String text = rs.getString("text");
	                int messageId = rs.getInt("message_id");
	                int userId = rs.getInt("user_id");
	                Timestamp createdDate = rs.getTimestamp("created_date");
	                Timestamp updatedDate = rs.getTimestamp("updated_date");


	                UserComment comment = new UserComment();
	                comment.setId(id);
	                comment.setName(name);
	                comment.setText(text);
	                comment.setMessageId(messageId);
	                comment.setUserId(userId);
	                comment.setCreatedDate(createdDate);
	                comment.setCreatedDate(updatedDate);

	                ret.add(comment);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	    }


}
