package ushigami_tatsuya.dao;

import static ushigami_tatsuya.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ushigami_tatsuya.beans.UserBranchDepartment;
import ushigami_tatsuya.exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	 public List<UserBranchDepartment> getUserBranchDepartment(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as user_id, ");
			sql.append("users.account as user_account, ");
			sql.append("users.name as user_name, ");
			sql.append("users.branch_id as branch_id, ");
			sql.append("branches.name as branch_name, ");
			sql.append("users.department_id as department_id, ");
			sql.append("departments.name as department_name, ");
			sql.append("users.is_stopped as is_stopped, ");
			sql.append("users.created_date as created_date, ");
			sql.append("users.updated_date as updated_date ");
			sql.append("FROM branches ");
			sql.append("INNER JOIN ( users ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ) ");
			sql.append("ON branches.id = users.branch_id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> ret = toUserBranchDepartmentList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toUserBranchDepartmentList(ResultSet rs)
			throws SQLException {

		List<UserBranchDepartment> ret = new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				int userId = rs.getInt("user_id");
				String userAccount = rs.getString("user_account");
				String userName = rs.getString("user_name");
				int branchId = rs.getInt("branch_id");
				String branchName = rs.getString("branch_name");
				int departmentId = rs.getInt("department_id");
				String departmentName = rs.getString("department_name");
				int isStopped = rs.getInt("is_stopped");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");


				UserBranchDepartment manegement = new UserBranchDepartment();
				manegement.setUserId(userId);
				manegement.setUserAccount(userAccount);
				manegement.setUserName(userName);
				manegement.setBranchId(branchId);
				manegement.setBranchName(branchName);
				manegement.setDepartmentId(departmentId);
				manegement.setDepartmentName(departmentName);
				manegement.setIsStopped(isStopped);
				manegement.setCreatedDate(createdDate);
				manegement.setUpdatedDate(updatedDate);

				ret.add(manegement);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
