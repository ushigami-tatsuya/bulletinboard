package ushigami_tatsuya.dao;

import static ushigami_tatsuya.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import ushigami_tatsuya.beans.Branch;
import ushigami_tatsuya.exception.SQLRuntimeException;

public class BranchDao {

	public List<Branch> getBranch(Connection connection) {
		 PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT * from branches");
            ps = connection.prepareStatement(sql.toString());
            
            ResultSet rs = ps.executeQuery();
			List<Branch> ret = toBranchList(rs);
			
			return ret;
			
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Branch> toBranchList(ResultSet rs) throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				Branch branch = new Branch();
				branch.setBranchId(id);
				branch.setName(name);
				branch.setCreatedDate(createdDate);
				branch.setUpdatedDate(updatedDate);

				ret.add(branch);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}