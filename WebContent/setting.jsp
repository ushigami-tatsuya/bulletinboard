<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE htmlPUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー編集画面</title>

        <style>
    body {
    	background: #eeeeee;
  		font-family: Meiryo;
  		}
  	#main-contents {
  		background: #ffffff;
  		width: 500px;
  		text-align: center;
  		margin: 25px auto;
  		border: 1px solid #cccccc;
	}

  </style>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="setting" method="post"><br />

			<a href="manegement">戻る</a><br />

			<label for="account">アカウント</label>
			<input name="account"id="account" value="${init.account}" /> <br />

			<label for="password">パスワード</label>
			<input name="password" id="password" /> <br />

			<label for="passwordcheck">確認用パスワード</label>
			<input name="passwordcheck" id="passwordcheck" /> <br />

			<label for="name">名前</label>
			<input name="name" id="name" value="${init.name}" /> <br />

			<label for="branch">支社</label>
			  <select name="branch_id" >
			    <c:forEach items="${branches}" var="branch" >
			    <c:if test="${branch.branchId == init.branchId }">
			    	<option value="${branch.branchId}"selected >"${branch.name}"</option>
    			</c:if>
    			<c:if test="${branch.branchId != init.branchId && init.id != user.id}">
    			  <option value="${branch.branchId}" >"${branch.name}"</option>
    			</c:if>
			    </c:forEach>
			  </select>

			<label for="department">部署</label>
			<select name="department_id">
			  <c:forEach items="${departments}" var="department" >
			  <c:if test="${department.departmentId == init.departmentId }">
    		    <option value="${department.departmentId}"selected >"${department.name}"</option>
    		  </c:if>
    		  <c:if test="${department.departmentId != init.departmentId && user.id!= init.id}">
    		    <option value="${department.departmentId}" >"${department.name}"</option>
    		  </c:if>
			  </c:forEach>
			</select>
			<input type="hidden"  name="id" value="${init.id}">
			<input type="submit" value="更新" /> <br />
		</form>
		<div class="copyright">Copyright(c) Tatsuya Ushigami</div>
	</div>
  </body>
</html>