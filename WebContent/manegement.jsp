<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー管理画面</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">

  </head>

  <body>
  	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">						<li><c:out value="${message}" />
				</c:forEach>
			</ul>
	    </div>
			<c:remove var="errorMessages" scope="session" />
	</c:if>

  　<div id="header">
 	　<a href="./">ホーム</a>
	  <a href="signup">ユーザー新規登録</a>
    </div>

  	<div id="manegement">
    	<c:forEach items="${manegements}" var="manegement">
			<div class="useraccount"><c:out value="${manegement.userAccount}" /></div>
            <div class="username"><c:out value="${manegement.userName}" /></div>
            <div class="branchname"><c:out value="${manegement.branchName}" /></div>
            <div class="departmentname"><c:out value="${manegement.departmentName}" /></div>
            <c:if test="${manegement.isStopped == 0 }">稼働中</c:if>
            <c:if test="${manegement.isStopped == 1 }">停止中</c:if>
            <div class="header">
 				<form action="setting" method="get">
	   		    	<input  type="hidden"  name="userId" value="${manegement.userId}">
	    			<input type="submit" value="編集">
	    		</form>
				<c:if test="${user.id != manegement.userId}">
				<c:if test="${manegement.isStopped == 1 }">
	    		<form name="is_stopped" action="stop" method="post" onsubmit="return check();">
	   		    	<input type="hidden"  name="userid" value="${manegement.userId}">
	   		    	<input type="hidden"  name="userisstopped" value="0">
	    			<input type="submit" value="復活">
	    		</form>
	    		</c:if>

				<c:if test="${manegement.isStopped == 0 }">
	    		<form name="is_started" action="stop" method="post"onsubmit="return check2();">
	    			<input type="hidden"  name="userid" value="${manegement.userId}">
	   		    	<input type="hidden"  name="userisstopped" value="1">
	    			<input type="submit" value="停止"><br />
	    		</form>
	    		</c:if>


              	<script type="text/javascript" >
          			function check() {
          				var result = window.confirm("このユーザーを復活しますか？");
 	 	   				if(result){
 	 	   					return true;
 	 		    		} else {
 	 		    			return false;
 	 		    		}
          			}
             	</script>
              	<script type="text/javascript" >
          			function check2() {
          				var result = window.confirm("このユーザーを停止しますか？");
 	 	   				if(result){
 	 	   					return true;
 	 		    		} else {
 	 		    			return false;
 	 		    		}
          			}
             	</script>
				</c:if>
	        </div>
     	</c:forEach>
    </div>

    <div class="copyright">Copyright(c) Tatsuya Ushigami</div>
  </body>
</html>