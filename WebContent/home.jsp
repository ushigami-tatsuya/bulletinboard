<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ホーム画面</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">

</head>
<body>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">						<li><c:out value="${message}" />
				</c:forEach>
			</ul>
	    </div>
			<c:remove var="errorMessages" scope="session" />
	</c:if>

 <div id="header">
 	    <a href="message">新規投稿</a>
	    <c:if test="${user.branchId == 1 && user.departmentId == 1}">
	    	<a href="manegement">ユーザー編集</a>
	    </c:if>
	    <a href="logout">ログアウト</a>

	<form action="./" method="get">
		<input type="date" name="start" value="${start}" >～
		<input type="date" name="end" value="${end}" >
		<input type="text" name="category" value="${category}">
		<input type="submit" value="絞り込み" ><br />
	</form>

	<div id="messages">
    	<c:forEach items="${messages}" var="message">
      		<div class="messageshow">
      		  <h4><label for="name">投稿内容</label></h4>
            	<div class="name"><c:out value="${message.name}" /></div>
                <div class="title"><c:out value="${message.title}" /></div>
                <div class="category"><c:out value="${message.category}" /></div>
               	<div class="text"><pre><c:out value="${message.text}" /></pre></div>
              	<div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
              </div>

              <div class="messagedelete">
              　<c:if test="${user.id == message.userId }">
                <form name="messagedelete" action="deletemessage" method="post"  onsubmit="return check();">
                 <input  type="hidden"  name="message_id" value="${message.id}">
	    		 <input type="submit" value="投稿削除" ><br />
                </form>
                </c:if>

              		<script type="text/javascript" >
              			function check() {
     	 				var result = window.confirm("この投稿を削除しますか？");
     	 	   			if(result){
     	 	   				return true;
     	 		    		} else {
     	 		    			return false;
     	 		    		}
              			}
                 	</script>

              </div>

            <div class="commentshow">
              <h4><label for="name">コメント</label></h4>
            	<c:forEach items="${comments}" var="comment">
            	  　<c:if test="${message.id == comment.messageId }">
						<div class="name"><c:out value="${comment.name}" /></div>
                		<div class="text"><pre><c:out value="${comment.text}" /></pre></div>
              			<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
              		<div class="commentdelete">
						<c:if test="${user.id == comment.userId }">
              		  	   <form name="commentdelete" action="deletecomment" method="post" onsubmit="return check2();">
	   		    		     <input  type="hidden"  name="comments_id" value="${comment.id}">
	    				     <input type="submit" value="コメント削除"><br />
	    			       </form>
	    			       </c:if>
	    			       </div>
						</c:if>
              	 　
              			<script type="text/javascript" >
              			function check2() {
     	 				var result = window.confirm("このコメントを削除しますか？");
     	 	   			if(result){
     	 	   				return true;
     	 		    		} else {
     	 		    			return false;
     	 		    		}
              			}
                 		</script>

                 		</c:forEach>
                 	</div>




            	<div class="comment">
			    	<form action="comment" method="post">
			   			<h4><label for="text">コメント入力欄</label></h4>
	   		     		<input name="text" id="text" value="${inittext}">
	    		    	<input  type="hidden"  name="messageid" value="${message.id}">
	    		    	<input type="submit" value="コメント投稿">（500文字まで）<br />
					</form>
				</div>

    	</c:forEach>
</div>
	  <div class="copyright">Copyright(c) Tatsuya Ushigami</div>
</div>
</body>
</html>