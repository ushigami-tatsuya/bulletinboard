<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE htmlPUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">

</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="message" method="post"><br />

		<a href="./">ホーム</a><br />

		<label for="title">件名</label>
		<input name="title" id="title" value="${init.title}"/> <br />

		<label for="category">カテゴリ</label>
		<input name="category" id="category" value="${init.category}"/> <br />

		<label for="text">投稿内容</label>
 		<input name="text" id="text" value="${init.text}"/>

		<input type="submit" value="投稿" /> <br />

		</form>
		<div class="copyright">Copyright(c) Tatsuya Ushigami</div>
	</div>
  </body>
</html>