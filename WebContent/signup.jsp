<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE htmlPUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー新規登録</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post"><br />

			<a href="manegement">ユーザー管理</a><br />

			<label for="account">アカウント</label>
			<input name="account"id="account" /> <br />

			<label for="password">パスワード</label>
			<input name="password" id="password" /> <br />

			<label for="passwordcheck">確認用パスワード</label>
			<input name="passwordcheck" id="passwordcheck" /> <br />

			<label for="name">名前</label>
			<input name="name" id="name" /> <br />

			<label for="branch">支社</label>
			  <select name="branch_id" >
			  　<option selected >-支社を入力してください-</option>
			    <c:forEach items="${branches}" var="branch" >
    			  <option value="${branch.branchId}" >"${branch.name}"</option>
			    </c:forEach>
			  </select>

			<label for="department">部署</label>
			<select name="department_id">
			　<option selected >-部署を入力してください-</option>
			  <c:forEach items="${departments}" var="department" >
    		    <option value="${department.departmentId}" >"${department.name}"</option>
			  </c:forEach>
			</select>
			<input type="submit" value="登録" /> <br />
		</form>
		<div class="copyright">Copyright(c) Tatsuya Ushigami</div>
	</div>
  </body>
</html>